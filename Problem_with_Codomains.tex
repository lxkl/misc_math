\newcommand*{\lxklVarTraditionalFunctions}{true}
\input{header}
\addbibresource{../bibliographies/teaching_en.bib}
\hypersetup{
  pdftitle={The Problem with Codomains},
  pdfauthor={Lasse Kliemann},
  pdfsubject={Why functions should not have codomains},
  pdfkeywords={function, codomain, surjective},
}

\begin{document}
\title{The Problem with Codomains}
\author{Lasse Kliemann}
\date{(see \nameref{sec:version-history})}
\maketitle

A function $f$ typically consists of three parts:

\begin{itemize}
\item a set called the \term{domain} of $f$, which we denote by $\dom(f)$;
\item a set called the \term{codomain} of $f$, which we denote by $\cod(f)$;
\item a \term{mapping rule}, which is some expression that,
  given $x \in \dom(f)$, names an element of $\cod(f)$,
  called the \term{image} of $x$ under $f$ and denoted by $f(x)$;
  the element $x$ is called the \term{argument} in this context,
  and we say that it is \term{mapped} to $f(x)$
  or that $f$ \term{assumes} the value $f(x)$ in $x$.
\end{itemize}
The \term{image} of a function $f$ is $\img(f) \df \set{f(x) \suchthat x \in \dom(f)} \subseteq \cod(f)$,
that is, the set of all values that $f$ assumes.

In this article, I argue that a function should instead only consist of a domain and a mapping rule,
no codomain.
I will show that:
\begin{itemize}
\item in most situations, the codomain is irrelevant;
\item when it is not irrelevant, we can look at the image instead;
\item if taken seriously, the codomain can make things extremely complicated and evoke serious problems,
  without providing any benefit.
\end{itemize}

\section{Preliminaries}

A common notation is $\ffn{f}{A}{B}$ for the statement
that $f$ is a function with $\dom(f) = A$ and $\cod(f) = B$.
A common notation to define a function is $\ffnx{f}{A}{B}{x \mapsto \hdots}$,
where $f$ is the name of the function, $A$ is the domain, $B$ is the codomain,
and $x \mapsto \hdots$ is the mapping rule; in the place of the three dots,
an expression is given by which $f(x)$ is determined.
Of course, any free symbol can be used instead of $x$.

\begin{samepage}
  Examples:
  \begin{itemize}
  \item $\ffnx{f}{\RR}{\RR}{x \mapsto x^{2}}$ is the function that maps
    each real to its square and which has codomain~$\RR$.
    The function $\ffnx{g}{\RR}{\RRnn}{x \mapsto x^{2}}$ maps
    each real to its square and has codomain~$\RRnn$,
    hence $f \neq g$.
  \item $\ffnx{f}{\RR}{\RRpos}{x \mapsto x^{2}}$ is not a function,
    because for argument~$0$, the mapping rules produces~$0$, which is no element of~$\RRpos$.
  \item $\ffnx{f}{\RR}{\RR}{x \mapsto \frac{1}{x}}$ is not a function,
    because the mapping rule cannot be applied to argument~$0$.
  \end{itemize}
\end{samepage}

A function $f$ is called \term{injective} if $f(a) = f(b)$ implies $a=b$ for all $a, b \in \dom(f)$.
This notion does not use the codomain, and we can decide whether a function is injective
without ever looking at its codomain.

A function $f$ is called \term{surjective} if $\img(f) = \cod(f)$.
A function $f$ is called \term{bijective} if $f$ is injective and surjective.
It is easy to prove:
\begin{proposition}
  \label{prop:bijective-inverse}
  A function $f$ is bijective if and only if
  there is $\ffn{g}{\cod(f)}{\dom(f)}$ with
  $g(f(x)) = x$ and $f(g(y)) = y$ for all $x \in \dom(f)$ and all $y \in \cod(f)$.
\end{proposition}
If such a function $g$ exists, it is uniquely determined and denoted by $f^{-1}$.

Examples:
\begin{itemize}
\item $\ffnx{f}{\RRnn}{\RR}{x \mapsto x^{2}}$ is injective but not surjective.
  The function $\ffnx{g}{\RRnn}{\RRnn}{x \mapsto x^{2}}$, which differs from $f$ only by its codomain,
  is bijective.
\item The \term{real exponential function}
  $\ffnx{\exp}{\RR}{\RRpos}{x \mapsto \sum_{k=0}^{\infty} \frac{x^{k}}{k!}}$
  is bijective, and its inverse function is denoted by $\ln$ and called the natural logarithm.
\end{itemize}

\section{What Do We Need Codomains For?}

The notion of surjectivity requires a codomain,
and we will deal with this in the next section in full detail.

Codomains are also sometimes used to express that a function does not assume certain values,
for example postulating $0 \not \in \cod(f)$
guarantees that $\frac{1}{f(x)}$ is defined for all $x \in \dom(f)$.
Of course, the same can be achieved by postulating $0 \not \in \img(f)$,
which is also more to the point:
we want to exclude that $f$ ever assumes~$0$,
so we postulate that $0$ is not in the set of values that $f$ assumes.

I am not aware of any other uses of codomains.

\section{Removing the Notion of Surjectivity}
\label{sec:remove-surjectivity}

\begin{proposition}
  \label{prop:injective-inverse}
  A function $f$ is injective if and only if
  there is $\ffn{g}{\img(f)}{\dom(f)}$ with
  $g(f(x)) = x$ and $f(g(y)) = y$ for all $x \in \dom(f)$ and all $y \in \img(f)$.
\end{proposition}

\begin{proof}
  Define $\ffnx{\tif}{\dom(f)}{\img(f)}{x \mapsto f(x)}$;
  this is $f$ where we replaced $\cod(f)$ by $\img(f)$.
  Clearly, $\tif$ is surjective, hence:
  \begin{IEEEeqnarray*}{0rCl"s}
    &&\text{$f$ is injective} \\*
    &\iff&
    \text{$\tif$ is bijective}
    & definition \\*
    &\iff&
    \exists \ffn{g}{\cod(\tif)}{\dom(\tif)} \innerholds
    \forall x \in \dom(\tif) \innerholds \forall y \in \cod(\tif) \holds
    & \autoref{prop:bijective-inverse} \\*
    && g(\tif(x)) = x \land \tif(g(y)) = y \\*
    &\iff&
    \exists \ffn{g}{\cod(\tif)}{\dom(\tif)} \innerholds
    \forall x \in \dom(\tif) \innerholds \forall y \in \cod(\tif) \holds
    & $\forall x \in \dom(\tif) \holds$ \\*
    && g(f(x)) = x \land f(g(y)) = y
    & $\tif(x) = f(x)$ \\*
    &\iff&
    \exists \ffn{g}{\img(f)}{\dom(f)} \innerholds
    \forall x \in \dom(f) \innerholds \forall y \in \img(f) \holds \\*
    && g(f(x)) = x \land f(g(y)) = y
  \end{IEEEeqnarray*}
  The last step holds since $\cod(\tif) = \img(f)$ and $\dom(\tif) = \dom(f)$.
\end{proof}

If $f$ is injective, the function $g$ in the preceding proposition is uniquely determined
and can easily take that place of the inverse in our thinking.
The proof shows how easy it is to \enquote{make} an injective function bijective:
keep the domain, keep the mapping rule, and reduce the codomain to the image.

In my experience, the notion of surjectivity causes a lot of trip\=/ups in introductory classes.
After understanding injectivity,
many students seem to intuitively conceive \autoref{prop:injective-inverse}, at least in spirit.
But surjectivity throws them off;
this notion appears contrived and unnecessary, and looking at \autoref{prop:injective-inverse},
I think we should admit that surjectivity probably is contrived and unnecessary indeed.
At the time of writing, for several semesters I taught mathematics, on different levels,
using codomain\-/free functions and the notion of inverse function given via \autoref{prop:injective-inverse}.
It works.

There is prior art for this:
in \cite{Stewart16}, Section~1.5, the inverse function is defined for injective functions;
injectivity is called \enquote{one-to-one} there, and the image is called the \enquote{range} there.
Very similar approaches are taken in:
\begin{itemize}
\item \cite{Spivak08}, Chapter~12, Theorem~1
\item \cite{Koenigsberger04Ana1}, Section 4.1 (in German)
\end{itemize}
Please let me know of further references.

The following three section demonstrate more problems with codomains,
some of them being serious.

\section{Restricting the Domain}

Define the \term{complex exponential function}
$\ffnx{\Exp}{\CC}{\CCnz}{z \mapsto \sum_{k=0}^{\infty} \frac{z^{k}}{k!}}$.
It would be nice to write $\fnres{\Exp}{\RR} = \exp$, but this is false,
since $\cod(\fnres{\Exp}{\RR}) = \CCnz$ and $\cod(\exp) = \RRpos$.
We would need a notation that also changes the codomain,
for example $\fnresx{\Exp}{\RR}{\RRpos} = \exp$.

So this issue can be fixed, although at the price of a more complicated notation.
The following two sections show more serious problems.

\section{Vector Space of Functions}
\label{sec:vector-space}

Denote $V \df \set{f \suchthat \dom(f) = \cod(f) = \RR}$.
For all $\lam \in \RR$ and all $f,g \in V$ define:
\begin{IEEEeqnarray*}{0l}
  \ffnx{f+g}{\RR}{\RR}{x \mapsto f(x) + g(x)} \\
  \ffnx{\lam \ccdot f}{\RR}{\RR}{x \mapsto \lam f(x)}
\end{IEEEeqnarray*}
With these operations, $V$ becomes a vector space over~$\RR$.
It will perhaps surprise the reader to note that $\exp \not\in V$.
Indeed, $\cod(\exp) = \RRpos$, but all functions in $V$ have codomain $\RR$.
We could have defined $\exp$ using $\RR$ as the codomain,
but then $\exp$ would not have been surjective and hence there would be no $\ln$
(unless we get rid of the notion of surjectivity as suggested in \autoref{sec:remove-surjectivity}).
So this is not an option.

Let us try to fix this, maintaining the notion of codomain.
Denote $W \df \set{f \suchthat \dom(f) = \RR \land \cod(f) \subseteq \RR}$.
Now $\exp \in W$.
But the above operations do not turn $W$ into a vector space anymore.
We have, closely observing the definition of addition, $f + \exp \neq \exp$ for all $f \in W$,
since $\cod(f + \exp) = \RR$, hence there is no neutral element.
Also, closely observing the definition of scalar multiplication,
we recognize that $\cod(1 \ccdot \exp) = \RR$, hence $1 \ccdot \exp \neq \exp$.
And so on; it is a mess.

The problem appears to be that the operations impose $\RR$ as codomain of $f+g$ and $\lam f$.
Define instead:
\begin{IEEEeqnarray*}{0l+l}
  \ffnx{f+g}{\RR}{B_{f+g}}{x \mapsto f(x) + g(x)}
  & B_{f+g} \df \set{f(x) + g(x) \suchthat x \in \RR} \\
  \ffnx{\lam \ccdot f}{\RR}{B_{\lam \ccdot f}}{x \mapsto \lam f(x)}
  & B_{\lam \ccdot f} \df \set{\lam f(x) \suchthat x \in \RR}
\end{IEEEeqnarray*}
There is still no neutral element.
The only candidates for neutral elements are the functions $\ffnx{\nu_{B}}{\RR}{B}{x \mapsto 0}$
with $0 \in B \subseteq \RR$.
But we have $\nu_{\RR} + \nu_{B} = \nu_{\set{0}} \neq \nu_{\RR}$,
so none of them is neutral.

The new codomains appear too restrictive, perhaps this is the problem.
Define instead:
\begin{IEEEeqnarray*}{0l+l}
  \ffnx{f+g}{\RR}{B_{f+g}}{x \mapsto f(x) + g(x)} \\*
  \eqin B_{f+g} \df \set{f(x) + g(x) \suchthat x \in \RR} \cup \cod(f) \cup \cod(g) \\
  \ffnx{\lam \ccdot f}{\RR}{B_{\lam \ccdot f}}{x \mapsto \lam f(x)} \\*
  \eqin B_{\lam \ccdot f} \df \set{\lam f(x) \suchthat x \in \RR} \cup \cod(f)
\end{IEEEeqnarray*}
Note how complicated this has become.
And it is still not a solution,
because now $\exp + \nu_{B} \neq \exp$, for all $0 \in B \subseteq \RR$,
since $0 \in \cod(\exp + \nu_{B})$ and ${0 \not \in \cod(\exp)}$.
So there is still no neutral element.

As a last attempt, define:
\begin{IEEEeqnarray*}{0l+l}
  \ffnx{f+g}{\RR}{B_{f+g}}{x \mapsto f(x) + g(x)} \\*
  \eqin B_{f+g} \df \set{f(x) + g(x) \suchthat x \in \RR} \cup \p{\cod(f) \cap \cod(g)} \\
  \ffnx{\lam \ccdot f}{\RR}{B_{\lam \ccdot f}}{x \mapsto \lam f(x)} \\*
  \eqin B_{\lam \ccdot f} \df \set{\lam f(x) \suchthat x \in \RR} \cup \cod(f)
\end{IEEEeqnarray*}
Finally, $\nu_{\RR}$ is neutral.
But trouble continues with additive inverse elements.
The only candidates for an additive inverse to $\exp$
are the functions $\ffnx{\mu_{B}}{\RR}{B}{x \mapsto -\exp(x)}$ with $\RRneg \subseteq B \subseteq \RR$.
We have $\cod(\exp + \mu_{B}) = \set{0} \cup (\RRpos \cap B) \subseteq \RRnn$,
hence $\exp + \mu_{B} \neq \nu_{\RR}$, regardless what $B$ is.

I will stop here.
If you have any idea how to fix this while maintaining the notion of codomain, please let me know.

\section{Derivatives}
\label{sec:derivatives}

Let $f$ be a function with $\dom(f) = \RR$ and $\cod(f) \subseteq \RR$,
and let $f$ be differentiable, that is,
there exists $f'(x) \df \flim{\hat{x}}{x} \frac{f(\hat{x}) - f(x)}{\hat{x} - x} \in \RR$ for all $x \in \RR$.
What codomain should $f'$ have?

\begin{itemize}
\item $\cod(f') = \RR$?
  Then $\exp' \neq \exp$, which most likely is not what we want.
\item $\cod(f') = \cod(f)$?
  This does not work for $\fnx{\RR}{\RRnn}{x \mapsto x^{2}}$.
\item $\cod(f') = \set{f'(x) \suchthat x \in \RR}$?
  Then all derivatives are surjective,
  hence $\fnx{\RR}{\RR}{x \mapsto x^{2}}$ does not admit an anti\-/derivative,
  which most likely is not what we want.
\item $\cod(f') = \set{f'(x) \suchthat x \in \RR} \cup \cod(f)$?
  Then $\fnx{\RR}{\RRnn}{x \mapsto x^{2}}$ does not admit an anti\-/derivative,
  because any such anti\-/derivative would have codomain~$\RR$.
\end{itemize}

I will stop here.
I do not think there is a solution, except for dropping the notion of codomain.
If you have any idea how to fix this while maintaining the notion of codomain, please let me know.

\section{Solution}

Just drop the notion of codomain and adopt the notion of inverse function provided by
\autoref{prop:injective-inverse}.
All the problems demonstrated above will go away.

Notation could be simplified to any of the following, where $A$ is the domain:
\begin{enumerate}
\item\label{item:notation:1} $f\colon \altfn{x}{A}{\hdots}$
\item\label{item:notation:2} $f\colon \tlafn{x}{A}{\hdots}$
\item\label{item:notation:3} $f\colon x \mapsto \hdots \quad (x \in A)$
\item\label{item:notation:4} $f(x) = \hdots \quad (x \in A)$
\end{enumerate}

The notation given in \ref*{item:notation:2} is perhaps the best
for inline notation of anonymous functions.
For example $\tlafn{x}{\RR}{x^{2}}$ is the function that maps each real to its square.
As always, we can use parenthesis to avoid ambiguities or for typographical reasons.
For example, we could write $\p{\tlafn{x}{\RR}{x^{2}}}$ to emphasize that this is one mathematical object.
Another example is $\tlafn{n}{\NN}{\p{\tlafn{x}{\RR}{x^{n}}}}$,
which can also be written as $\p{\tlafn{n}{\NN}{\p{\tlafn{x}{\RR}{x^{n}}}}}$,
and which is the function that maps each positive integer $n$ to the function
that maps each real to its $n$th power.

While we are at it, we can as well make notation more systematic in another aspect too.
Instead of the colon after the function name,
we use standard notation for definition and statement of equality.
To \emphasis{define} $f$ as a certain function, write $f \df \p{\tlafn{x}{A}{\hdots}}$.
To \emphasis{state} that a \emphasis{previously defined} object $f$ equals a certain function,
write $f = \p{\tlafn{x}{A}{\hdots}}$.

There is prior art for this:
in \cite{Lamport03}, Section~5.2, our notation \ref*{item:notation:2} is introduced
(using square brackets there).

By $\tlafnset{A}{B}$ we denote the set of all functions $f$ with $\dom(f) = A$
and ${\img(f) \subseteq B}$.
Note $\tlafnset{A}{B} \subseteq \tlafnset{A}{B'}$ for all $B \subseteq B'$.
This notation (with square brackets) is also used in~\cite{Lamport03}.

Examples:
\begin{itemize}
\item $\p{\tlafn{x}{\RRnn}{x^{2}}} \in \tlafnset{\RRnn}{\RRnn} \subseteq \tlafnset{\RRnn}{\RR}$
\item $\p{\tlafn{x}{\RR}{(x+1)^{2}}} = \p{\tlafn{x}{\RR}{x^{2}+2x+1}}$
\item $\p{\tlafn{x}{\RRnn}{x^{2}}}^{-1} = \p{\tlafn{x}{\RRnn}{\sqrt{x}}}$
\item $\img\p{\tlafn{x}{\RRpos}{-\frac{1}{x}}} = \intio{0}$
\item $\p{\tlafn{x}{\RR}{x^{2}}}' = \p{\tlafn{x}{\RR}{2x}}$
\item $\tlafnset{\RR}{\RR}$ becomes a vector space over $\RR$ when endowed with the operations:
  \begin{IEEEeqnarray*}{0l}
    f+g \df \p{\tlafn{x}{\RR}{f(x) + g(x)}} \\
    \lam \ccdot f \df \p{\tlafn{x}{\RR}{\lam f(x)}}
  \end{IEEEeqnarray*}
  For example:
  \begin{IEEEeqnarray*}{0l}
    \p{\tlafn{x}{\RR}{x-1}} + \p{\tlafn{x}{\RR}{x^{2}}} = \p{\tlafn{x}{\RR}{x^{2} + x - 1}} \\
    2 \ccdot \p{\tlafn{x}{\RR}{x-1}} = \p{\tlafn{x}{\RR}{2x-2}}
  \end{IEEEeqnarray*}
\item $\p{\tlafn{x}{\RRnn}{\sqrt{x}}} \circ \p{\tlafn{x}{\RR}{x^{2} + 1}}
  = \p{\tlafn{x}{\RR}{\sqrt{x^{2} + 1}}}$
\end{itemize}

\section{Functions as Sets}

It can be argued that the mapping rule is a somewhat informal concept.
The following helps.
A set $f$ is called a \term{function} if there are sets $A$ and $B$
such that $f \subseteq A \times B$ and $f$ is \term{right\-/unique}, that is,
if for all $(a,b), (a,c) \in f$ we have $b=c$.
Domain and image are easily defined:
\begin{IEEEeqnarray*}{0l+l}
  \dom(f) \df \set{x \in A \suchthat \exists y \holds (x,y) \in f} &
  \img(f) \df \set{y \in B \suchthat \exists x \holds (x,y) \in f}
\end{IEEEeqnarray*}
Clearly, $\dom(f)$ and $\img(f)$ are independent of the choice of $A$ and $B$,
as long as $f \subseteq A \times B$.
It makes sense to define the \term{inverse} of $f$ as $f^{-1} \df \set{(y,x) \suchthat (x,y) \in f}$,
regardless whether $f$ is injective or not.
It is easy to see:

\begin{proposition}
  $f^{-1}$ is a function if and only if the function $f$ is injective.
\end{proposition}

\section{Discussion}

We have seen how the notion of codomain evokes serious problems
(\autoref{sec:vector-space} and \autoref{sec:derivatives})
and can safely be disposed of even in the context of inverse functions
(\autoref{sec:remove-surjectivity}).
There seems to be a silent agreement in the mathematical community to ignore codomains
most of the time, and to consider two function equal if and only if they
have the same codomain and the same mapping rule.
Only when it comes to surjectivity and inverse functions,
attention is paid to codomains.
This may work well for most purposes if done by seasoned mathematicians.
But it understandably causes confusion in introductory classes,
and it is unnecessary inconsequential.
Even the notion of surjectivity, although not having any contradictions in itself,
is often perceived as contrived and superfluous by students.
Indeed, a concept of inverse function can be introduced just fine using only
domain, mapping rule, image, and injectivity (\autoref{prop:injective-inverse}).

When considering functions as sets in the codomain\-/free framework,
matters become particularly elegant.
A function then is simply a right\-/unique subset of some cartesian product;
all other parameters, such as domain and image, can be deduced from this set.

\textbf{Conclusion:}
I suggest that we drop the notion of codomain;
a function should simply consist of a domain and a mapping rule.
Prior art for this is given in \cite{Lamport03,Koenigsberger04Ana1,Spivak08,Stewart16},
which includes references from calculus and computer science.
As notation, I suggest using the one from \cite{Lamport03},
but without the square brackets, that is, $\tlafn{x}{A}{\hdots}$ or $\p{\tlafn{x}{A}{\hdots}}$,
where $A$ is the domain.

We could say that a function $f$ is \term{surjective on} a set $B$ if $B = \img(f)$,
or, maybe, if $B \subseteq \img(f)$.
For example, a permutation on a set $S$ is a function in $\tlafnset{S}{S}$
that is injective and surjective on $S$.
We could also say that a function $f$ is \term{bijective} between a set $A$ and a set $B$
if $\dom(f) = A$ and $\img(f) = B$ and $f$ is injective.
Then a permutation on a set $S$ is a function that is bijective between $S$ and $S$.
Or a vector space isomorphism between vector spaces $V$ and $W$ is a linear map
that is bijective between $V$ and $W$.
This way, a text which frequently uses the surjectivity or bijectivity notion with respect to a codomain
can be quickly adapted to the codomain\-/free framework.

\printbibliography

\pagebreak
\section{Version History}
\label{sec:version-history}

\begin{description}
\item[2022-07-17:] first release, request for comments
\end{description}

\pagebreak\thispagestyle{empty}
\input{lxklCreativeCommons}
\input{lxklNoPaperShort}

\end{document}
