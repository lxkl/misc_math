\textbf{Decimal Representation.}
A commonly used form of representation is the decimal representation,
and we will only deal with this one in this document, namely in the following form.
We call a quadruple $R = (\sig, b, c, d)$ a \term{representation} where
\begin{itemize}
\item $\sig \in \set{-,+}$ is the \term{sign},
\item $b = (\eli{b}{\len{b}})$ and $c = (\eli{c}{\len{c}})$
  are vectors with entries from the set $\setft{0}{9}$ and with $\len{b} \in \NNone$ and $\len{c} \in \NNzero$,
  which together form the \term{mantissa},
\item $c \in \ZZ$ is the \term{exponent}.
\end{itemize}
A more common form of writing out such a representation is:
\begin{IEEEeqnarray*}{0l}
  \sig \, \neli{b}{\len{b}} \,.\, \neli{c}{\len{c}} \cdot 10^{d}
\end{IEEEeqnarray*}
The dot between $b_{\len{b}}$ and $c_{1}$ is called the \term{decimal separator};
in some culture groups, a comma is used instead of a dot.
If $\len{c}=0$, then the decimal separator is omitted.
If $\sig = +$, then it may be omitted.

We define the \term{essential part} of the representation $R$, denoted $\eps(R)$,
as the vector that we obtain by removing all zeros at the beginning and the end
of the vector $(\eli{b}{\len{b}}, \eli{c}{\len{c}})$.



The number \term{represented} by $R$ is:
\begin{IEEEeqnarray*}{0l}
  n(R) \df \sig \, \p{\sum_{i=1}^{\ell(b)} b_{i} 10^{\ell(b)-i} + \sum_{i=1}^{\ell(c)} c_{i} 10^{-i}} \cdot 10^{d}
\end{IEEEeqnarray*}
Not every number in $\RR$ can be represented in this way.
But for practical purposes, this form of representation is sufficient.
Clearly, there are representations $R \neq R'$ with $n(R) = n(R')$.
We say that a representation is \term{normalized} if $\len{b} = 1$ and $b_{1} \in \setft{1}{9}$.
We say that $R$ has a \term{trailing zero} at position $i \leq \len{c}$ if $b_{j} = 0$ for all $j \geq i$.

It is easy to see that if $R$ and $R'$ are normalized representations without any trailing zeros,
then $R=R' \iff n(R)=n(R')$.
It is also easy to see that for each representation $R$ with $n(R) \neq 0$,
there is a normalized representation $R'$ without trailing zeros
such that $\eps(R) = \eps(R')$ and $n(R) = n(R')$.
The transformation is basically achieved by adjusting the exponent.

\textbf{Semantic Aspect.}
Let $\Del_{1}, \Del_{2} \in \RR^{\NN}$, which we call \term{margin functions}.
Let $y \neq 0$ be a number admitting a decimal representation $(\sig, \p{y_{i}}_{r \leq i \leq l})$.
Let $k \in \NN = \set{1,2,3,\hdots}$, and let $n \in \ZZ$ be maximal under the condition $y_{n} \neq 0$.
We say that $y$ has $k$ \term{significant digits with respect to} a number~$x$ if:
\begin{IEEEeqnarray*}{0l+l}
  x \in \intncc{\hat{y}-\Del_{1}(n-k)}{\hat{y}+\Del_{2}(n-k)}
  & \text{where} \quad \hat{y} \df \sig \sum_{\max\set{n-k, \, r-1} < i \leq n} y_{i} 10^{i}
\end{IEEEeqnarray*}
That is, starting at the leftmost non\-/zero digit of $y$,
we collect $k$ digits (or less, if the decimal representation does not offer enough digits at the right end)
and consider the number $\hat{y}$ represented by those digits,
then we build an interval around $\hat{y}$ using the margin functions,
and $x$ is guaranteed to be contained in this interval.
The collected digits are also called the \term{significant digits} of $y$.
Note that without the \enquote{with respect to} part, this notion does not make sense.
In practice, $x$ is often a quantity in a model,
for example the barometric pressure at a particular location at a particular point in time.

\pagebreak

A \term{(finite) decimal representation} is a pair $(\sig, \vec{u})$ where $\sig \in \set{-,+}$
and $\vec{u} = \p{u_{i}}_{r \leq i \leq l}$ is a finite family of numbers in the set $\setft{0}{9}$
with $r, l \in \ZZ$ with $l \leq 0 \leq r$.
Let $(\sig, \vec{u})$ be a decimal representation.
The number $\sig \sum_{r \leq i \leq l} u_{i} 10^{i}$ is called the number \term{represented} by $(\sig,\vec{u})$.
We call $u_{i}$ the $i$th \term{digit} of the representation.
We can write out the decimal representation in the form:
\begin{IEEEeqnarray*}{0l}
  \sig u_{l} u_{l-1} \cdots u_{0} \, . \, u_{-1} \cdots u_{r}
\end{IEEEeqnarray*}
The dot between $u_{0}$ and $u_{-1}$ is called the \term{decimal separator};
in some culture groups, a comma is used instead of a dot.
If $r=0$, then the decimal separator is omitted.
If $\sig = +$, then it may be omitted.

If $i > 0$ and $u_{j} = 0$ for all $j \geq i$,
then we call the $i$th digit of the decimal representation a \term{leading zero}.
If $0 > i$ and $u_{j} = 0$ for all $i \geq j$,
then we call the $i$th digit of the decimal representation a \term{trailing zero}.
It is easy to see that two decimal representations are equal up to leading and trailing zeros
if and only if they represent the same number.
(Recall that we only consider finite decimal representations.)
Clearly, adding or removing leading or trailing zeros to or from a decimal representation
does not change the represented number.

Often useful is the \term{scientific notation}.
Let $b \in \ZZ$. Then the notation
\begin{IEEEeqnarray*}{0l}
  \sig v_{l} v_{l-1} \cdots v_{0} \, . \, v_{-1} \cdots v_{r} \, \cdot \, 10^{b}
\end{IEEEeqnarray*}
with $r \leq 0 \leq l$ means the decimal representation $(\sig,\vec{u})$,
where $\vec{u} = \p{u_{i}}_{r+b \leq i \leq l+b}$ is defined
by $u_{i} \df v_{i-b}$ for all $r+b \leq i \leq l+b$.
Hence, the represented number is $\sig \sum_{r \leq i \leq l} v_{i} 10^{i+b}$.

\textbf{Semantic Aspect.}
Let $\Del_{1}, \Del_{2} \in \RR^{\NN}$, which we call \term{margin functions}.
Let $y \neq 0$ be a number admitting a decimal representation $(\sig, \p{y_{i}}_{r \leq i \leq l})$.
Let $k \in \NN = \set{1,2,3,\hdots}$, and let $n \in \ZZ$ be maximal under the condition $y_{n} \neq 0$.
We say that $y$ has $k$ \term{significant digits with respect to} a number~$x$ if:
\begin{IEEEeqnarray*}{0l+l}
  x \in \intncc{\hat{y}-\Del_{1}(n-k)}{\hat{y}+\Del_{2}(n-k)}
  & \text{where} \quad \hat{y} \df \sig \sum_{\max\set{n-k, \, r-1} < i \leq n} y_{i} 10^{i}
\end{IEEEeqnarray*}
That is, starting at the leftmost non\-/zero digit of $y$,
we collect $k$ digits (or less, if the decimal representation does not offer enough digits at the right end)
and consider the number $\hat{y}$ represented by those digits,
then we build an interval around $\hat{y}$ using the margin functions,
and $x$ is guaranteed to be contained in this interval.
The collected digits are also called the \term{significant digits} of $y$.
Note that without the \enquote{with respect to} part, this notion does not make sense.
In practice, $x$ is often a quantity in a model,
for example the barometric pressure at a particular location at a particular point in time.

Define particular margin functions, namely $\Del_{1}(t) \df \Del_{2}(t) \df 5 \cdot 10^{t}$
with $t \in \NN$.
We call those the \term{standard margin functions}.
It is easy to see that if we use the standard margin functions
and the number $y$ is given in scientific notation,
we can first construct the interval as if the $10^{b}$ part was not there
and then add $10^{b}$ to the lower and upper bound of the interval.

We give an example using the standard margin functions.
Assume that $y = 0.00202137$ hat $4$ significant digits with respect to some number~$x$.
Using the above notation, $l=-3$ and $k=4$ and $\hat{y} = 0.002021$
and $\Del_{1}(-7)=\Del_{2}(-7) = {5 \cdot 10^{-7}} = 0.0000005$.
Thus ${x \in \intncc{0.0020205}{0.0020215}}$.
Note that, by definition, the \enquote{$37$} part of $y$ is irrelevant for the construction of the interval.
If however, instead, we have $k=7$ significant digits, then $\hat{y} = y$,
and the margin is $\Del_{1}(-10)=\Del_{2}(-10)=5 \cdot 10^{-10}$.
We thus know ${x \in \intncc{0.0020213695}{0.0020213705}}$.

\needspace{2\baselineskip}%
More examples are given in the following table, always using the standard margin functions:

\begin{tabular}{llll}%
  \toprule
  {number $y$} & {sign. digits} & lower bound & upper bound \\
  \midrule
  $1234500$ & $3$ & $1225000$ & $1230000$ \\
               & $4$ & $1233500$ & $1234500$ \\
               & $5$ & $1234450$ & $1234550$ \\
  $-5432100$ & $3$ & $-5435000$ & $-5425000$ \\
               & $4$ & $-5432500$ & $-5431500$ \\
               & $5$ & $-5432150$ & $-5432050$ \\
  $12.39 \cdot 10^{2}$ & $3$ & $12.25 \cdot 10^{2}$ & $12.35 \cdot 10^{2}$ \\
               & $4$ & $12.385 \cdot 10^{2}$ & $12.395 \cdot 10^{2}$ \\
               & $5$ & $12.38995 \cdot 10^{2}$ & $12.3905 \cdot 10^{2}$ \\
  $7.081 \cdot 10^{-5}$ & $3$ & $7.075 \cdot 10^{-5}$ & $7.085 \cdot 10^{-5}$ \\
               & $4$ & $7.0805 \cdot 10^{-5}$ & $7.0815 \cdot 10^{-5}$ \\
               & $5$ & $7.08095 \cdot 10^{-5}$ & $7.08105 \cdot 10^{-5}$ \\
  \bottomrule
\end{tabular}%
\par\medskip

\textbf{Syntactic Aspect.}
Instead of explicitly stating the number of significant digits,
we could agree on a convention, by which the number of significant digits can be deduced
from the representation in which $y$ is given.
We propose the following convention.
It uses more than just the decimal representation of $y$,
namely it uses the representation of $y$ in scientific notation $y = c \cdot 10^{b}$,
where $c \neq 0$ has a decimal representation, and $b \in \ZZ$.
If $b=0$, the \enquote{$\, \cdot 10^{b}$} may be omitted.
Let $(\sig, \p{c_{i}}_{r \leq i \leq l})$ be the decimal representation of $c$,
and let $n$ be minimal under the condition $c_{n} \neq 0$.

We distinguish two cases:
\begin{description}[font=\it]
\item[Case 1:] The decimal representation of $c$ has a decimal separator
  (after which at least one more digit follows), that is, $0 > r$.
  Then the number of significant digits is assumed to be $n-r+1$.
\item[Case 2:] The decimal representation of $c$ has no decimal separator
  (that is, $r=0$) and $c_{r} \neq 0$.
  Then the number of significant digits is assumed to be $n$.
\end{description}
In other words, in both of the above cases,
the leftmost non\-/zero digit and all digits right of it are assumed to be significant.
If neither of the above cases holds,
then no assumption regarding number of significant digits is made.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For all $c \in \RR$ and $c \in B \subseteq \RR$ define $\ffnx{\kappa_{c,B}}{\RR}{B}{x \mapsto c}$.
Then the only candidates for neutral elements are the functions $\kappa_{0,B}$
with $0 \in B \subseteq \RR$.
