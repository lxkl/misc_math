\input{../header}
\renewcommand*{\len}[1]{\ell\parens{#1}}

\begin{document}
\title{A Note on Significant Digits}
\author{Lasse Kliemann}
\date{(see \nameref{sec:version-history})}
\maketitle

Significant digits (or significant figures)
is a basic topic in the mathematical foundation of natural and applied sciences.
Remarkably, I could not find any clear explanation in the existing literature.
Many sources try \enquote{definition by example} (which is no definition at all),
or they confuse what I will call the
\term{semantic}, the \term{syntactic}, and the \term{procedural} aspects.
In this document, I will give rigorous definitions,
which hopefully capture what most authors actually mean when they speak of \enquote{significant digits}.

The basic problem is: a number $y$ is being communicated (in some representation),
while a possibly different number $x$ is actually meant,
and readers need to be able to construct an interval $I$ of reasonable length
from the communicated representation of $y$ such that $x \in I$ holds.
By \term{number}, we always mean a real number, that is, an element of $\RR$.
Why would we not communicate $x$ in the first place?
At least two reasons are conceivable for this and occur regularly in practice:
(i) $x$ is not known,
or (ii) $y$ admits a shorter representation than $x$.

We start with some preparations (decimal representation)
and then explain the three aspects (semantic, syntactic, procedural).
Finally, we give some Python code that implements the semantic and syntactic aspects.
Some of the formalism that I deploy may look unnecessarily complicated at first,
but I believe that it is necessary to avoid any ambiguities
(and we do not need another document on this topic that contains ambiguities).

\section{Decimal Representation}

A commonly used form of representation is the decimal representation,
and we will only deal with this one in this document, namely in the following form.
We distinguish between a number and its decimal representation.
We define a \term{decimal representation}, or \term{representation} for short,
as a construction of the form
\begin{IEEEeqnarray}{0l}
  \label{eq:repr}
  \sig \neli{a}{m} \, . \, \nelix{a}{m+1}{n} \cdot 10^{b}
\end{IEEEeqnarray}
with $\sig \in \set{+,-}$ (the \term{sign})
and $m,n \in \NN = \set{1,2,3,\hdots}$
and $a_{i} \in \setft{0}{9}$ for all $i \in \setn{n} = \setft{1}{n}$
and $b \in \ZZ$ (the \term{exponent}).
We call the numbers in $\setn{n}$ the \term{digits},
and $a_{i}$ is the \term{value} of digit~$i$ or of the $i$th digit for all $i \in \setn{n}$.
For all $i \in \NN$, we define the \term{weight} $w_{i} = 10^{m-i+b}$.
The \term{number represented} by~\eqref*{eq:repr} is $\sig \sum_{i=1}^{n} a_{i} w_{i}$ of course.
Not every number in $\RR$ can be represented in this way.
For example, $\frac{1}{3}$ has no such representation
and could only be approximately represented in this way, for example as $0.3333333$.
But for many practical purposes, a representation of the form~\eqref*{eq:repr} is sufficient.

We define the \term{first significant digit} $s$
to be the smallest number in $\setn{n}$ such that $a_{s} \neq 0$,
or to be $1$ if $a_{i} = 0$ for all $i \in \setn{n}$.
It is easy to see that the represented number is~$0$ (that is, $\sig \sum_{i=1}^{n} a_{i} w_{i} = 0$)
if and only if~${a_{s}=0}$.
Clearly, $\sig \sum_{i=1}^{n} a_{i} w_{i} = \sig \sum_{i=s}^{n} a_{i} w_{i}$.

The dot between $a_{m}$ and $a_{m+1}$ is called the \term{decimal separator};
in some culture groups, a comma is used instead of a dot.
Note that the dots in $\neli{a}{m}$ and $\nelix{a}{m+1}{n}$ are not part of the representation,
but merely indicate ellipses.
That is, $\neli{a}{m}$ means that we have $a_{1}$, then $a_{2}$, and so on,
up to and including $a_{m}$; likewise for $\nelix{a}{m+1}{n}$.
If $m=n$, then the decimal separator is omitted,
so the representation becomes $\sig \neli{a}{n} \cdot 10^{b}$.
Note that there is always at least one digit to the left of the decimal separator.
If $\sig=+$, then $\sig$ may be omitted.
If $b=0$, then the \enquote{$\cdot 10^{b}$} part may be omitted.

\section{Semantic Aspect}

Let a representation in the form of~\eqref*{eq:repr} be given with first significant digit~$s \in \setn{n}$.
Let $k \in \NN$ and denote $d = \min\set{s+k-1, \, n}$.
We say that the representation \term{has $k$ significant digits with respect to} a number~$x$
or that the representation is \term{to $k$ significant digits with respect to} a number~$x$ if:
\begin{IEEEeqnarray*}{0l+l}
  x \in \intncc{\hat{y} - 5 w_{s+k}}{\hat{y} + 5 w_{s+k}}
  & \text{where} \quad \hat{y} = \sig \sum_{i=s}^{d} a_{i} w_{i}
\end{IEEEeqnarray*}
The digits starting at $s$ up to and including $d$ are called the \term{significant digits};
these determine $\hat{y}$.
Recall that $s$ was aptly named the \enquote{first significant digit}.

We give an example.
Assume that $0.00202137$ has $k=4$ significant digits with respect to some number~$x$.
We have $m=1$ and $n=9$ and $b=0$ and $s=4$ and $d=7$ and $\hat{y} = 0.002021$,
and the radius of the interval is $5w_{8} = \np{5e-7}$.
Thus ${x \in \intncc{0.0020205}{0.0020215}}$.
Note that, by definition, the \enquote{$37$} part in the given representation is irrelevant
for the construction of the interval.
If, however, we have $k=7$ significant digits, then $d=9$ and $\hat{y} = 0.00202137$,
and the radius of the interval is $w_{11}=\np{5e-10}$.
We thus know ${x \in \intncc{0.0020213695}{0.0020213705}}$.

Note that without the \enquote{with respect to} part, the notion introduced here does not make sense.
In practice, $x$ is often a quantity in a model,
for example the barometric pressure at a particular location at a particular point in time.
Example statement: \enquote{The barometric pressure at \dots
  was $132000$ Pascal, to $3$ significant digits.}
Now we know that the said pressure in Pascal is in the interval $\intncc{131500}{132500}$.

\needspace{2\baselineskip}%
More examples are given in the following table:

\begin{tabular}{llll}%
  \toprule
  {representation} & {sign. digits} & lower bound & upper bound \\
  \midrule
  $1234500$ & $3$ & $1225000$ & $1235000$ \\
                   & $4$ & $1233500$ & $1234500$ \\
                   & $5$ & $1234450$ & $1234550$ \\
  $-5432100$ & $3$ & $-5435000$ & $-5425000$ \\
                   & $4$ & $-5432500$ & $-5431500$ \\
                   & $5$ & $-5432150$ & $-5432050$ \\
  $\np{12.39e2}$ & $3$ & $\np{12.25e2}$ & $\np{12.35e2}$ \\
                   & $4$ & $\np{12.385e2}$ & $\np{12.395e2}$ \\
                   & $5$ & $\np{12.3895e2}$ & $\np{12.3905e2}$ \\
  $\np{7.081e-5}$ & $3$ & $\np{7.075e-5}$ & $\np{7.085e-5}$ \\
                   & $4$ & $\np{7.0805e-5}$ & $\np{7.0815e-5}$ \\
                   & $5$ & $\np{7.08095e-5}$ & $\np{7.08105e-5}$ \\
  \bottomrule
\end{tabular}%
\par\bigskip

It is easy to see that if $R$ and $R'$ are representations of the form~\eqref*{eq:repr}
representing the same number,
then we obtain the same intervals from $R$ as we do from $R'$.
For example, $\np{7.081e-5}$ and $0.00007081$ give the same intervals.

\section{Syntactic Aspect}

Instead of explicitly stating the number of significant digits,
we could agree on a convention, by which the number of significant digits can be deduced
from the given representation.
Based on the existing literature, we propose the following convention.
Again, let $s$ denote the first significant digit.

Consider the following three conditions:
\begin{enumerate*}
\item\label{cond:1}
  $a_{s} \neq 0$ and $m < n$,
  that is, there is a digit with $\neq 0$ value,
  and we have a decimal separator (after which at least one digit follows).
\item\label{cond:2}
  $m=n$ and $a_{n} \neq 0$, that is,
  there is no decimal separator, and the rightmost digit's value is $\neq 0$.
\item\label{cond:3}
  The represented number is~$0$ (in which case $s=1$ and $a_{s}=0$).
\end{enumerate*}
If one of these conditions holds, the number of significant digits is assumed to be $n-s+1$,
that is, all the digits starting at $s$ up to and including $n$ are significant.
In other words, the only non\-/significant digits are those strictly left of~$s$.
Note that under each of the first two conditions, the represented number is $\neq 0$.

If none of the three conditions holds,
no assumption regarding the number of significant digits is made.

The convention in case of conditions~\ref*{cond:1} and~\ref*{cond:2}
is pretty standard in the existing literature.
The case of the represented number being~$0$,
here covered by our convention via condition~\ref*{cond:3},
usually is not considered at all.
But I feel that what I propose here is reasonable.
It means that, for example, in $0.000$, all $4$ digits are considered significant.
Note that $0.000$ and $00000000.000$ and $\np{0e-3}$
each induce the same interval by an application of first the convention
and then the semantic aspect,
namely $\intncc{-0.0005}{0.0005}$.
If you do not like this, you could remove condition~\ref*{cond:3}
and deal with number~$0$ in some other way.

The leftover cases, that is, representations of the form
\begin{IEEEeqnarray}{0l}
  \label{eq:repr:2}
  \sig \neli{a}{t} \underbrace{0 \hdots 0}_{\text{at least one~$0$}} \cdot \: 10^{b}
\end{IEEEeqnarray}
with $t \in \NN$ and $a_{i} \neq 0$ for at least one $i \in \setn{t}$, require further discussion.
Examples are $100$ and $\np{100e5}$ and $321000$.
The rationale given in the literature
for not making an assumption regarding the number of significant digits in this case
is as follows:
\enquote{We do not know if the zeros at the right end of the representation
  are there (i)~to be considered as significant digits,
  or, if (ii)~they are just there to push the digits with $\neq 0$ values to the left so that
  they receive their intended weights.}
I would see no harm in always assuming~(i).
In case of~(ii), authors could use the exponent to eliminate the zeros that should not be considered significant.
If, for the above examples, none of the zeros should be considered significant,
appropriate representations would be $\np{1e2}$ and $\np{1e7}$ and $\np{321e3}$.
Note that instead of $\np{321e3}$, we could also write $\np{3.21e5}$,
which carries the exact same message.
If in $321000$, only the first zero should be considered significant,
we could write $\np{3210e2}$.

However, it seems to be the prevailing opinion
to be extra careful with representations of the form~\eqref*{eq:repr:2}
and not to assume anything.
We should follow this.

Using the exponent, it is always possible (without changing the represented number)
to bring a representation into a form for which our convention makes a statement.
This works by either eliminating all the zeros at the right end of the representation
or by introducing a decimal separator.
For example, if we like to express that in $100$, the first two digits should be considered significant,
we write $\np{1.0e2}$.

\needspace{2\baselineskip}%
We give examples for our convention in the following table.
The second column gives the number of significant digits
that is deduced from the representation as per the convention,
or \enquote{--} if the convention does not make a statement.
Lower bound and upper bound are calculated as per the semantic aspect.

\begin{tabular}{llll}%
  \toprule
  {representation} & {sign. digits} & {lower bound} & {upper bound} \\
  \midrule
  $0.123456789$   & 9 & $0.1234567885$   & $0.1234567895$ \\
  $123456789$     & 9 & $123456788.5$    & $123456789.5$ \\
  $1234.56789$    & 9 & $1234.567885$    & $1234.567895$ \\
  $1234.00000$    & 9 & $1233.999995$    & $1234.000005$ \\
  $0.0010004$     & 5 & $0.00100035$     & $0.00100045$ \\
  $0.00100040$    & 6 & $0.001000395$    & $0.001000405$ \\
  $0.001$         & 1 & $0.0005$         & $0.0015$ \\
  $0.00100$       & 3 & $0.000995$       & $0.001005$ \\
  $\np{3.333e-1}$ & 4 & $\np{3.3325e-1}$ & $\np{3.3335e-1}$ \\
  $\np{1.750e10}$ & 4 & $\np{1.7495e10}$ & $\np{1.7505e10}$ \\
  $1.414$         & 4 & $1.4135$         & $1.4145$ \\
  $\np{1.2340e8}$ & 5 & $\np{1.23395e8}$ & $\np{1.23405e8}$ \\
  $123400000$     & -- & -- & -- \\
  \bottomrule
\end{tabular}%
\par\bigskip

\textbf{Summary.}
The semantic aspect is about constructing an interval for the number~$x$ actually meant,
given a representation and the extra information on how many significant digits this representation has
with respect to~$x$.
The syntactic aspect is about conventions on how to deduce this extra information
from the representation.

\section{Procedural Aspect}

Recall that in the introduction,
we indicated that sometimes we like to shorten a representation.
This will typically introduce some error.
The task is
\begin{itemize}
\item to keep this error low,
\item to produce a representation that is
  accessible to our convention for determining the number of significant digits
  (syntactic aspect),
\item to ensure that the new representation indeed has the number of significant digits
  deducible via the convention with respect to the original number (semantic aspect).
\end{itemize}
We describe a possible procedure for this.
Let $x \neq 0$ be a number and let a representation of $x$ in the form of~\eqref*{eq:repr} be given.
Let $k \in \NN$ with $k \geq 2$.
The steps of the procedure are:
\begin{enumerate}
\item Bring the representation into \term{normalized form},
  that is, adjust the exponent and move the decimal separator so that to the left of the decimal separator,
  there is exactly one digit and this digit has $\neq 0$ value.
  It is easy to see that this step is always possible.
  Let the new representation be $\sig a_{1} \, . \, \nelix{a}{2}{n} \cdot 10^{b}$.
\item\label{item:step:2}
  \begin{enumerate}[label=(\alph*)]
  \item\label{item:step:2:a}
    If $n-1 \leq k-1$, that is, if there are $k-1$ or less digits to the right of the decimal separator,
    add $k-n$ zeros at the right end of the representation and go to the next step.
  \item Otherwise, bring the representation to exactly $k-1$ digits to the right of the decimal separator
    by common rounding: remove digit $k+1$ and all digits to the right of it,
    and if $a_{k+1} \in \setft{5}{9}$, that is, if the first removed digit has value $5$ or higher,
    add $1$ to the value of digit~$k$, taking care of add carry if necessary.
    If the value of a digit becomes~$0$ as a result of this,
    keep this digit with value~$0$ in the representation.
    However, if due to add carry the number of digits increases,
    finally remove the rightmost zero; example: $9.9999$ and $k=4$
    will first yield $10.000$, and then we remove the final zero to obtain $10.00$.
    We also adjust the exponent and move the decimal separator to go back to normalized form;
    in the example we would switch to $\np{1.000e1}$.
  \end{enumerate}
\item The preceding steps yield a normalized form with exactly $k$ significant digits as per the convention,
  and indeed due to the way we round,
  this representation has this number of significant digits with respect to~$x$.
  We may keep this representation or adjust it to our taste by changing the exponent
  and moving the decimal separator.
  If we make changes, we need to keep in mind that
  we are allowed to add zeros at the left end of the representation
  but not at the right end of it.
\end{enumerate}

The following table gives examples for $k=4$.
The assumed representation of $x$ is in the left column.
As an example in which way we might want to change the normalized form,
in the last column we provide a representation with exponent~$0$ if possible.

\begin{tabular}{ln{1}{3}l}%
  \toprule
  input & {rounded in normalized form} & {rounded with exponent $0$} \\
  \midrule
  $0.123456789$ & 1.235e-1 & $0.1235$ \\
  $123456789$ & 1.235e8 & -- \\
  $1234.56789$ & 1.235e3 & $1235$ \\
  $0.0010004$ & 1.000e-3 & $0.001000$ \\
  $0.001$     & 1.000e-3 & $0.001000$ \\
  $\np{3.3333333e-1}$ & 3.333e-1 & $0.3333$ \\
  $\np{875e-3}$ & 8.750e-1 & $0.8750$ \\
  $1.41421356237$ & 1.414 & $1.414$ \\
  $10.9544511501$ & 1.095e1 & $10.95$ \\
  \bottomrule
\end{tabular}%
\par\bigskip

\needspace{\baselineskip}%
A word of caution is in order regarding the adding of zeros at the right end of the representation,
as it is possibly done in step~\ref*{item:step:2}~\ref*{item:step:2:a} in the procedure.
Assume we weigh an object with a scale, and the scale shows $1.3$ gram.
If we put this representation into the procedure with $k=4$, we obtain $1.300$.
Would it be wise to report the weight of the object with $1.300$ gram?
No, most probably not.
We must keep in mind that $1.300$ has $4$ significant digits with respect to $1.3$,
but not necessarily with respect to the weight of the object!
In this situation, we should investigate the accuracy of the scale to
learn how $1.3$ gram relates to the weight of the object.
The format in which we report the weight of the object should then be chosen accordingly.

\section{Python Implementation}

The following Python class \lstinline|Representation| implements the semantic and syntactic aspect.
It can be used to verify the examples from the corresponding sections,
as shown at the end of the code.
A text file with the code is available at
\url{\lxklVarGitRepos/tree/master/Significant_Digits/representation_class.py}.

\lstinputlisting[firstline=2]{representation_class.py}

\pagebreak
\section{Version History}
\label{sec:version-history}

\begin{description}
\item[2020-04-03:] first release, request for comments
\end{description}

\pagebreak\thispagestyle{empty}
\input{lxklCreativeCommons}
\input{lxklNoPaperShort}

\end{document}
