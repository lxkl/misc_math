#!/usr/bin/env python3

from fractions import Fraction


class Representation:
    def __init__(self, a_str, b):
        a = []
        m = None
        if a_str[0] == '-':
            self.sign = -1
            a_str = a_str[1:]
        else:
            self.sign = 1
        for i in range(1, len(a_str)+1):
            if a_str[i-1] == '.':
                m = i-1
            else:
                x = int(a_str[i-1])
                a += [x]
        n = len(a)
        if m is None: m = n
        s = 1
        for i in range(1, n+1):
            if a[i-1] != 0:
                s = i
                break
        self.a = a; self.b = b
        self.m = m; self.n = n; self.s = s

    def w(self, i):
        x = self.m - i + self.b
        if x < 0:
            return(Fraction(1, 10**(-x)))
        else:
            return(Fraction(10**x, 1))

    def represented_number(self):
        res = 0
        for i in range(1, self.n + 1):
            res += self.a[i-1] * self.w(i)
        res = self.sign * res
        return(float(res))

    def number_significant_digits(self):
        if ((self.a[self.s-1] != 0 and self.m < self.n)
           or (self.m == self.n and self.a[self.n-1] != 0)
           or self.a[self.s-1] == 0):
            return(self.n - self.s + 1)
        else:
            return(None)

    def interval(self, k):
        d = min([self.s + k - 1, self.n])
        y_hat = 0
        for i in range(self.s, d+1):
            y_hat += self.a[i-1] * self.w(i)
        y_hat = self.sign * y_hat
        l = y_hat - 5 * self.w(self.s + k)
        r = y_hat + 5 * self.w(self.s + k)
        return([float(l), float(r)])


def demo(y, k):
    if k is None:
        print('{} -- --'.format(y.represented_number()))
    else:
        print('{} {} {}'.format(y.represented_number(), k, y.interval(k)))


demo(Representation("0.00202137", 0), 4)
demo(Representation("0.00202137", 0), 7)

for y in [Representation("1234500", 0), Representation("-5432100", 0),
          Representation("12.39", 2), Representation("7.081", -5)]:
    for k in [3, 4, 5]: demo(y, k)

for y in [Representation("0.123456789", 0), Representation("123456789", 0),
          Representation("1234.56789", 0), Representation("1234.00000", 0),
          Representation("0.0010004", 0), Representation("0.00100040", 0),
          Representation("0.001", 0), Representation("0.00100", 0),
          Representation("3.333", -1), Representation("1.750", 10),
          Representation("1.414", 0), Representation("1.2340", 8),
          Representation("123400000", 0)]:
    demo(y, y.number_significant_digits())
