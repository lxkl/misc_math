\newcommand*{\lxklVarTheoremNumberWithin}{section}
\newcommand*{\lxklVarTheoremNumbersFirst}{true}
\input{header}
\addbibresource{../bibliographies/teaching_en.bib}
\input{../Skripten/notation2}

\begin{document}
\title{Definitions of Expectation}
\author{Lasse Kliemann}
\date{(see \nameref{sec:version-history})}
\maketitle

The expectation $\expect{X}$ of a random variable $X$ (of any type)
is sometimes defined via discretization,
that is, $\expect{X} = \limx{n} \expect{\frac{\floor{nX}}{n}}$,
and sometimes via integrals over the CDF $F_{X}$, that is,
$\expect{X} = \int_{0}^{\infty} (1-F_{X}) - \int_{\ninfty}^{0} F_{X}$.
In this note, I collect proofs to show that the two definitions yield the same.
This is all pretty standard,
but I found no place in the literature where all of this is in one place.

\section{Basics of Summable Families}

The expecation for discrete random variables involves summable families,
so we summarize some basic results here.

\begin{para}[Definition]
  For a set $M$, denote by $\finsets{M}$ the set of all finite subsets of~$M$,
  that is $\finsets{M} = \set{N \subseteq M \suchthat \card{N} \in \NNzero}$.
  Let $I$ be a set and $\famii{a_{i}}$ a family of reals.
  We call $\famii{a_{i}}$ \term{summable} with \term{sum} $s \in \RR$, if:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \innerholds
    \exists F_{0} \in \finsets{I} \innerholds
    \forall F \in \finsets{I} \with F_{0} \subseteq F \holds
    \abs{s - \sum_{i \in F} a_{i}} < \veps
  \end{IEEEeqnarray*}
  It is easy to see that each summable family has exactly one sum,
  which we denote by $\sum_{i \in I} a_{i}$.
\end{para}

\begin{para}[Boundedness Criterion]
  \label{boundedness}
  A family $\famii{a_{i}}$ of reals is summable if and only if
  $\set{\sum_{i \in F} \abs{a_{i}} \suchthat F \in \finsets{I}}$ is bounded.
  In particular, all subfamilies of a summable family are summable.
\end{para}

\begin{proof}
  See \enquote{Hauptkriterium für Summierbarkeit} in \cite[Section~6.3]{Koenigsberger04Ana1}
\end{proof}

The following statement follows immediately:

\begin{para}[Summability under Absolute Value]
  \label{sum-abs}
  A family $\famii{a_{i}}$ of reals is summable if and only if
  the family of its absolute values $\famii{\abs{a_{i}}}$ is summable.
\end{para}

\begin{para}[Upper Bound for Non-negative Families]
  \label{upper-non-negative}
  Let $\famii{a_{i}}$ be a summable family of non\=/negative reals.
  Then all $F \in \finsets{I}$,
  we have $\sum_{i \in F} a_{i} \leq \sum_{i \in I} a_{i}$.
\end{para}

\begin{proof}
  Denote $s \df \sum_{i \in I} a_{i}$.
  Assume otherwise, that is, there is $F \in \finsets{I}$ such that
  $\veps \df \sum_{i \in F} a_{i} - s > 0$.
  Choose $F_{0} \in \finsets{I}$ as in the definition of summability with respect to $\veps$.
  Then we obtain a contradiction:
  \begin{IEEEeqnarray*}{0l+x*}
    \eps > \abs{\sum_{i \in F_{0} \cup F} a_{i} - s}
    = \abs{\sum_{i \in F_{0} \setminus F} a_{i} + \sum_{i \in F} a_{i} - s}
    = \abs{\sum_{i \in F_{0} \setminus F} a_{i} + \veps}
    \geq \veps & \QEDhere{1}%
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Partition Criterion]
  Let $\famii{a_{i}}$ be a family of reals,
  and let $\famik{I_{k}}$ be a partition of $I$,
  that is, $\dotbigcup_{k \in K} I_{k} = I$.
  Assume that:
  \begin{itemize}
  \item $\fami{a_{i}}{i}{I_{k}}$ is summable for all $k \in K$.
  \item The family of sums $\famik{\sum_{i \in I_{k}} a_{i}}$ is summable.
  \end{itemize}
  Then $\famii{a_{i}}$ is summable.
\end{para}

\begin{proof}
  For each $k \in K$ denote $s_{k} \df \sum_{i \in I_{k}} \abs{a_{i}}$,
  which exists by Summability under Absolute Value (\ref{sum-abs}).
  For all $F \in \finsets{I}$, using the Upper Bound for Non-negative Families
  (\ref{upper-non-negative}) two times we have:
  \begin{IEEEeqnarray*}{0l}
    \sum_{i \in F} \abs{a_{i}}
    = \sum_{\substack{k \in K\\F \cap I_{k} \neq\emptyset}} \sum_{i \in F \cap I_{k}} \abs{a_{i}}
    \leq \sum_{\substack{k \in K\\F \cap I_{k} \neq\emptyset}} s_{k} \leq \sum_{k \in K} s_{k}
  \end{IEEEeqnarray*}
  The claim now follows from the Boundedness Criterion (\ref{boundedness}).
\end{proof}

\begin{para}[Associative Property]
  Let $\famii{a_{i}}$ be summable,
  and let $\famik{I_{k}}$ be a partition of $I$.
  Then $\famik{\sum_{i \in I_{k}} a_{i}}$ is summable and:
  \begin{IEEEeqnarray*}{0l}
    \sum_{i \in I} a_{i} = \sum_{k \in K} \p{\sum_{i \in I_{k}} a_{i}}
  \end{IEEEeqnarray*}
\end{para}

\begin{proof}
  See \enquote{Großer Umordnungssatz} in \cite[Section~6.3]{Koenigsberger04Ana1}
\end{proof}

\begin{para}[Scaling Property]
  \label{scaling}
  Let $\famii{a_{i}}$ be summable and $\lam \in \RR$.
  Then $\famii{\lam a_{i}}$ is summable and:
  \begin{IEEEeqnarray*}{0l}
    \sum_{i \in I} \lam a_{i} = \lam \sum_{i \in I} a_{i}
  \end{IEEEeqnarray*}
\end{para}

\begin{proof}
  The case $\lam = 0$ is clear, so assume $\lam \neq 0$.
  Let $\veps > 0$ and choose $I_{0} \in \finsets{I}$ such that:
  \begin{IEEEeqnarray*}{0l}
    \forall F \in \finsets{I} \with I_{0} \subseteq F \holds
    \abs{s - \sum_{i \in F} a_{i}} < \frac{\veps}{\abs{\lam}}
  \end{IEEEeqnarray*}
  It follows for all $F \in \finsets{I}$ with $I_{0} \subseteq F$:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{\lam s - \sum_{i \in F} \lam a_{i}} < \veps
    \iff \abs{\lam} \abs{s - \sum_{i \in F} a_{i}} < \veps
    \iff \abs{s - \sum_{i \in F} a_{i}} < \frac{\veps}{\abs{\lam}}
    \iff \trueprop & \QEDhere{1}%
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Monotonicity]
  \label{mono}
  Let $\famii{a_{i}}$ and $\famii{b_{i}}$ be both summable
  with $a_{i} \leq b_{i}$ for all $i \in I$.
  Then $\sum_{i \in I} a_{i} \leq \sum_{i \in I} b_{i}$.
\end{para}

\begin{proof}
  Denote $s \df \sum_{i \in I} a_{i}$ and $t \df \sum_{i \in I} b_{i}$.
  For contradiction, assume $\veps \df s - t > 0$.
  Choose $I_{0}, I_{1} \in \finsets{I}$ such that:
  \begin{IEEEeqnarray*}{0l}
    \forall F \in \finsets{I} \with I_{0} \subseteq F \holds
    \abs{s - \sum_{i \in F} a_{i}} < \frac{\veps}{2} \\
    \forall F \in \finsets{I} \with I_{1} \subseteq F \holds
    \abs{t - \sum_{i \in F} b_{i}} < \frac{\veps}{2}
  \end{IEEEeqnarray*}
  Define $F \df I_{0} \cup I_{1}$.
  A contradiction follows:
  \begin{IEEEeqnarray*}{0l+x*}
    \veps = s - t \leq s - t + \sum_{i \in F} b_{i} - \sum_{i \in F} a_{i}
    \leq \abs{s - \sum_{i \in F} a_{i}} + \abs{t - \sum_{i \in F} b_{i}}
    < \veps & \QEDhere{1}%
  \end{IEEEeqnarray*}
\end{proof}

\section{Discrete Case}

\begin{para}[Discrete Random Variable]
  For any random variable $X$,
  define its \term{support} as $\supp(X) \df \set{x \in \RR \suchthat \prob{X=x} > 0}$;
  it can be shown easily that $\supp(X)$ is countable without any restrictions on $X$.
  We call $X$ a \term{discrete} random variable if $\prob{X \in \supp(X)} = 1$.
  Clearly, $X$ is already discrete if $\img(X)$ is countable.
  If $X$ is discrete and $f$ is a measurable function, then also $f(X)$ is a discrete random variable.
\end{para}

\begin{para}[Definition of Expectation]
  Let $X$ be a discrete random variable.
  If the family $\seq{x \ccdot \prob{X=x}}{x \in \supp(X)}$ is summable,
  then we say that $X$ admits an expectation,
  and we define the \term{expectation} $\expect{X}$ of $X$ as the sum of this family, that is:
  \begin{IEEEeqnarray*}{0l}
    \expect{X} = \sum_{x \in \supp(X)} x \ccdot \prob{X=x}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Splitting Argument]
  The $\sig$\=/additivity in a probability space has a direct connection to summability.
  Let $\famii{A_{i}}$ be a disjoint, countable family of events.
  Then by $\sig$\=/additivity, the family $\famii{\prob{A_{i}}}$ is summable,
  and moreover $\prob{\bigcup_{i \in I} A_{i}} = \sum_{i \in I} \prob{A_{i}}$.
  \par
  Assume now that the discrete random variable $X$ admits an expectation,
  and let $Y$ be another discrete random variable.
  For all $x \in \supp(X)$, $\sig$\=/additivity and the Scaling Property (\ref{scaling}),
  the following family is summable:
  \begin{IEEEeqnarray*}{0l}
    \seq{x \ccdot \prob{X=x \land Y=y}}{y \in \supp(Y)}
  \end{IEEEeqnarray*}
  Its sum is $x \ccdot \prob{X=x}$.
  Since $X$ admits an expectation, the family of these sums,
  which is $\seq{x \ccdot \prob{X=x}}{x \in \supp(X)}$ is summable.
  By the Partition Criterion, also the following family is summable:
  \begin{IEEEeqnarray*}{0l}
    \seq{x \ccdot \prob{X=x \land Y=y}}{(x,y) \in \supp(X) \times \supp(Y)}
  \end{IEEEeqnarray*}
  By the associative property, its sum is $\expect{X}$.
  \par
  If $Y$ also admits an expectation,
  then we can make the same argument with $X$ and $Y$ switched,
  so we get that the following family is summable:
  \begin{IEEEeqnarray*}{0l}
    \seq{y \ccdot \prob{X=x \land Y=y}}{(x,y) \in \supp(X) \times \supp(Y)}
  \end{IEEEeqnarray*}
  Its sum is $\expect{Y}$.
\end{para}

\begin{para}[Monotonicity of Expectation]
  \label{mono-exp}
  Let $X$ and $Y$ be discrete random variables, each admitting an expectation
  such that $\prob{X \leq Y} = 1$.
  Then $\expect{X} \leq \expect{Y}$.
\end{para}

\begin{proof}
  Using the Splitting Argument and Monotonicity (\ref{mono}):
  \begin{IEEEeqnarray*}{0l+x*}
    \expect{X}
    = \sum_{(x,y) \in \supp(X) \times \supp(Y)} x \ccdot \prob{X=x \land Y=y} \\
    = \sum_{\substack{(x,y) \in \supp(X) \times \supp(Y)\\x\leq y}} x \ccdot \prob{X=x \land Y=y} \\
    \leq \sum_{\substack{(x,y) \in \supp(X) \times \supp(Y)\\x\leq y}} y \ccdot \prob{X=x \land Y=y} \\
    = \sum_{(x,y) \in \supp(X) \times \supp(Y)} y \ccdot \prob{X=x \land Y=y} \\
    = \expect{Y} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Notation]
  For all $A \subseteq \RR$, denote:
  \begin{IEEEeqnarray*}{0l+l+l}
    A^{+} \df A \cap \RRpos &
    A^{-} \df A \cap \RRneg &
    -A \df \set{-x \suchthat x \in A}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Expectation under Absolute Value]
  A discrete random variable $X$ admits an expectation if and only if
  $\abs{X}$ admits and expectation.
  If so, then $\expect{X} \leq \expect{\abs{X}}$.
\end{para}

\begin{proof}
  The \enquote{if so} statement follows from Monotonicity of Expectation (\ref{mono-exp}).
  \par
  We have:
  \begin{IEEEeqnarray*}{0l}
    \supp(\abs{X}) = \set{\abs{x} \suchthat x \in \supp(X)} \subseteq \RRnn
  \end{IEEEeqnarray*}
  First, let $X$ admit an expectation.
  For all $F \in \finsets{\supp(\abs{X})}$, we have:
  \begin{IEEEeqnarray*}{0l}
    \sum_{x \in F} \abs{x} \ccdot \prob{\abs{X}=x} \\
    = \sum_{x \in F} x \ccdot \p{ \prob{X=x} + \prob{X=-x} } \\
    = \sum_{x \in F} x \ccdot \prob{X=x} + \sum_{x \in F} x \ccdot \prob{X=-x} \\
    = \sum_{x \in F} x \ccdot \prob{X=x} + \sum_{x \in -F} \abs{x} \ccdot \prob{X=x} \\
    = \sum_{x \in F \cup -F} \abs{x} \ccdot \prob{X=x} \\
    = \sum_{x \in (F \cup -F) \cap \supp(X)} \abs{x} \ccdot \prob{X=x} \\
    \leq \sum_{x \in \supp(X)} \abs{x} \ccdot \prob{X=x}
  \end{IEEEeqnarray*}
  Now let $\abs{X}$ admit an expectation.
  For all $F \in \finsets{\supp(X)}$, we have:
  \begin{IEEEeqnarray*}{0l+x*}
    \sum_{x \in F} \abs{x} \ccdot \prob{X=x}
    \leq \sum_{x \in F} \abs{x} \ccdot \prob{\abs{X}=\abs{x}} \\
    = \sum_{x \in F^{+}} \abs{x} \ccdot \prob{\abs{X}=\abs{x}}
    + \sum_{x \in F^{-}} \abs{x} \ccdot \prob{\abs{X}=\abs{x}} \\
    = \sum_{x \in F^{+}} \abs{x} \ccdot \prob{\abs{X}=x}
    + \sum_{x \in -F^{-}} \abs{x} \ccdot \prob{\abs{X}=x} \\
    \leq 2 \sum_{x \in \supp(\abs{X})} \abs{x} \ccdot \prob{\abs{X}=x}
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Expectation Comparison Test]
  Let $X,Y$ be discrete random variables such that $Y$ admits an expectation,
  and $\prob{\abs{X} \leq Y} = 1$.
  Then $X$ admits an expectation, and $\expect{X} \leq \expect{Y}$.
\end{para}

\begin{proof}
  The last statement follows from Monotonicity of Expectation (\ref{mono-exp}).
  \par
  For all $F \in \finsets{\supp(\abs{X})}$, we have:
  \begin{IEEEeqnarray*}{0l+x*}
    \sum_{x \in F} \abs{x} \ccdot \prob{\abs{X}=x} \\
    = \sum_{x \in F} \sum_{\substack{y \in \supp(Y)\\x \leq y}} x \ccdot \prob{\abs{X}=x \land Y=y} \\
    \leq \sum_{x \in F} \sum_{\substack{y \in \supp(Y)\\x \leq y}} y \ccdot \prob{\abs{X}=x \land Y=y} \\
    = \sum_{\substack{y \in \supp(Y)\\0 \leq y}} y \sum_{x \in F} \prob{\abs{X}=x \land Y=y} \\
    \leq \sum_{\substack{y \in \supp(Y)\\0 \leq y}} y \ccdot \prob{Y=y} \\
    \leq \sum_{y \in \supp(Y)} \abs{y} \ccdot \prob{Y=y} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{General Case: Discretization}

\begin{para}[Discretizing]
  Let $X$ be a random variable (of any type)
  and $\Om$ the sample space of the underlying probability space.
  For all $n \in \NNone$, define $X_{n} \df \frac{\floor{nX}}{n}$.
  Then $X_{n}$ is a random variable,
  since $\tlafn{x}{\RR}{\frac{\floor{nX}}{n}}$ is measurable.
  Since $\img(X_{n})$ is countable, $X_{n}$ is discrete.
  \par
  We also observe for all $n,m$ and all $\om \in \Om$:
  \begin{IEEEeqnarray*}{0l}
    % X_{n} \leq X = \frac{n X}{n} < \frac{\floor{n X} + 1}{n} = X_{n} + \frac{1}{n}
    X_{n}(\om) \leq X(\om) = \frac{m X(\om)}{m} < \frac{\floor{m X(\om)} + 1}{m} = X_{m}(\om) + \frac{1}{m}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[One Expectation Implies All]
  If there exists $m$ such that $X_{m}$ admits an expectation,
  then $X_{n}$ admits an expectation for all $n$,
  and in such a case, $\seq{\expect{X_{n}}}{n}$ is a Cauchy sequence.
\end{para}

\begin{proof}
  TODO
\end{proof}

\begin{para}[Definition]
  If $X_{n}$ admits an expectation for some $n \in \NNone$ and hence for all $n \in \NNone$,
  we say that $X$ admits an expectation and define:
  \begin{IEEEeqnarray*}{0l}
    \expect{X} \df \lim_{n} \expect{X_{n}}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Properties Carry Over]
  We see how the three main properties proven for discrete random variables
  in the previous section carry over:
  \begin{description}[font=\it]
  \item[Monotonicity:] If $\prob{X \leq Y} = 1$, then $\prob{X_{n} \leq Y_{n}} = 1$ for all $n$.
    The claim follows from monotonicity of the limit.
  \item[Absolute Value:] We have:
    \begin{IEEEeqnarray*}{0l}
      \text{$X$ admits expectation} \\
      \iff \text{$X_{n}$ admits expectation for all $n$} \\
      \iff \text{$\abs{X_{n}}$ admits expectation for all $n$} \\
      \iff \text{TODO}
    \end{IEEEeqnarray*}
  \item[Comparison Test:] TODO
  \end{description}
\end{para}

\section{Integral Formula}

TODO

\printbibliography

\pagebreak
\section{Version History}
\label{sec:version-history}

\begin{description}
\item[2025-03(Mar)-xx:] first release
\end{description}

\pagebreak\thispagestyle{empty}
\input{lxklCreativeCommons}
% \input{lxklNoPaper}

\end{document}
