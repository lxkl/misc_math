#include <assert.h>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

class MassFractionTo {
  public:
  MassFractionTo() {
    std::ifstream file("Data1976VectorA.txt");
    assert(file.is_open());
    std::string line;
    while (std::getline(file, line)) {
      std::stringstream ss(line);
      double value;
      if (ss >> value) { vectorA.push_back(value); }
      else { throw std::runtime_error("Invalid data in vector A file."); }
    }
    file.close();
    assert(vectorA.size() == 12);
    densityEthanol = density(1.0);
    assert(densityEthanol == 789.2391232999144);
  }

  double density(const double mf) const {
    double sum = 0;
    for (int i=0; i<=11; ++i) sum += vectorA[i] * std::pow(mf, i);
    return(sum);
  }

  double volumeConcentration(const double mf) const {
    return(mf * density(mf) / densityEthanol);
  }

  private:
  std::vector<double> vectorA;
  double densityEthanol;
};

MassFractionTo massFractionTo;
const uint64_t volumeConcentrationResolution = 1000;

std::vector<uint64_t> massFractionNumerators(const uint64_t n) {
  std::vector<uint64_t> res;
  const double n_dbl = static_cast<double>(n);
  const double volumeConcentrationResolution_dbl = static_cast<double>(volumeConcentrationResolution);
  double pvc = 0;
  uint64_t j = 1;
  double tvc = j / volumeConcentrationResolution_dbl;
  for (uint64_t i=1; i<=n; ++i) {
    const double mf = i / n_dbl;
    const double nvc = massFractionTo.volumeConcentration(mf);
    if ((pvc <= tvc) && (tvc <= nvc)) {
      if (tvc-pvc < nvc-tvc) { res.push_back(i-1); }
      else { res.push_back(i); }
      ++j;
      tvc = j / volumeConcentrationResolution_dbl;
    }
    pvc = nvc;
  }
  if (not ((res.size() == volumeConcentrationResolution)
           && (j == volumeConcentrationResolution + 1))) {
    std::cout << "WARNING: massFractionNumerators: did not get all the values" << std::endl;
  }
  return res;
}

void writeTable(std::ofstream &file, const uint64_t n) {
  file << "VolC	MassF	Dens" << std::endl;
  const double n_dbl = static_cast<double>(n);
  for (uint64_t i : massFractionNumerators(n)) {
    const double mf = i / n_dbl;
    const double vc_100 = 100 * massFractionTo.volumeConcentration(mf);
    const double mf_100 = 100 * mf;
    const double dens = massFractionTo.density(mf);
    file << std::setprecision(std::numeric_limits<double>::digits10 + 2)
         << vc_100 << "	" << mf_100 << "	" << dens << std::endl;
  }
}

int main() {
  for (int k=4; k<=9; ++k) {
    std::stringstream fileName;
    fileName << "DataTableMy" << k << "_cpp.tsv";
    std::ofstream file(fileName.str());
    assert(file.is_open());
    writeTable(file, std::pow(10, k));
    file.close();
  }
  return 0;
}
