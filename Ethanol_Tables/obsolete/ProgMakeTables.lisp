(require 'asdf)

(setf *read-default-float-format* 'double-float)

(defparameter *volume-concentration-resolution* 1000)

(defun file->vector (fn)
  (map 'vector #'read-from-string (uiop:read-file-lines fn)))

(defparameter *data-1976-vector-A* (file->vector "Data1976VectorA.txt"))

;; This is much slower without coercing to double-float. The relative
;; difference in results is of the order 10^-13, which should not
;; matter. So it appears reasonable to use the faster version.
(defun mass-fraction->density (mf)
  (let ((mf (coerce mf 'double-float)))
    (loop :for i :from 0 :to 11 :sum
          (* (aref *data-1976-vector-A* i) (expt mf i)))))

(defparameter *density-ethanol* (mass-fraction->density 1))

(defun mass-fraction->volume-concentration (mf)
  (/ (* mf (mass-fraction->density mf))
     *density-ethanol*))

(defun mass-fraction-numerators (n)
  (let ((j 1)
        (pvc 0)
        (nvc nil)
        (tvc (/ 1 *volume-concentration-resolution*))
        (res (make-array *volume-concentration-resolution* :fill-pointer 0)))
    (do ((i 1 (1+ i)))
        ((< n i))
      (setf nvc (mass-fraction->volume-concentration (/ i n)))
      (when (and (<= tvc nvc) (<= pvc tvc))
        (unless (< (fill-pointer res) *volume-concentration-resolution*)
          (error "ERROR: mass-fraction-numerators: no space in result arrray"))
        (vector-push (if (< (- tvc pvc) (- nvc tvc)) (1- i) i) res)
        (incf j)
        (setf tvc (/ j *volume-concentration-resolution*)))
      (setf pvc nvc))
    (unless (and (= (fill-pointer res) *volume-concentration-resolution*)
                 (= j (1+ *volume-concentration-resolution*)))
      (format t "WARNING: mass-fraction-numerators: did not get all the values~%"))
    res))

(defun write-table (stream n)
  (format stream "VolC	MassF	Dens~%")
  (let ((nums (mass-fraction-numerators n)))
    (dotimes (l (length nums))
      (let ((mf (/ (aref nums l) n)))
        (format stream "~F	~F	~F~%"
                (* 100 (mass-fraction->volume-concentration mf))
                (* 100 mf)
                (mass-fraction->density mf))))))

(defun main ()
  (loop :for k :from 4 :to 9 :do
        (with-open-file (stream (format nil "DataTableMy~A.tsv" k)
                                :direction :output
                                :if-exists :supersede
                                :if-does-not-exist :create)
          (write-table stream (expt 10 k)))))
