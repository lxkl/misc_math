\input{../header}
\addbibresource{../../bibliographies/teaching_en.bib}
\addbibresource{../../bibliographies/teaching_de.bib}
\hypersetup{
  pdftitle={A Numerical Look at Ethanol Tables},
  pdfauthor={Lasse Kliemann},
}

\newcommand*{\rhopol}{\rho_{\mathrm{76}}}
\newcommand*{\rhoeth}{\rho_{\mathrm{Eth}}}
\newcommand*{\round}{\operatorname{round}}
\newcommand*{\signif}{\operatorname{signif}}

\begin{document}
\title{A Numerical Look at Ethanol Tables}
\author{Lasse Kliemann}
\date{(see \nameref{sec:version-history})}
\maketitle

\section{Basic terms: properties of a solution}

Consider a solution of two liquid substances (our focus will be on ethanol and water later).
Interesting properties include:

\begin{itemize}
\item Mass $m$, volume $V$, and density $\rho = \frac{m}{V}$.
  We assume $m,V > 0$, hence also $\rho > 0$.
\item Mass fraction with respect to substance number~1 (will be ethanol later),
  which we denote by $w$.
  It is defined as $w = \frac{m_{1}}{m}$,
  where $m_{1}$ is the mass of substance number~1 contained in the solution.
  That is, if all the molecules of substance number~1 were collected from the solution,
  their total mass would be $m_{1}$, or, by definition $wm$.
\item Volume concentration with respect to substance number~1,
  which we denote by $\sig$.
  It is defined as $\sig = \frac{V_{1}}{V}$,
  where $V_{1}$ is the volume of substance number~1 contained in the solution.
  That is, if all the molecules of substance number~1 were collected from the solution,
  their total volume would be $V_{1}$, or, by definition~$\sig V$.
\end{itemize}

Note that volume, density, and volume concentration can vary with temperature.
For our ethanol\-/water studies, we fix temperature at $\SI{20}{\degree}$.

\paragraph{Conversion by first principles.}
Denote $\rho_{1} > 0$ the density of substance number~1,
that is, the density of a pure liquid of this substance.
We have $wm = \rho_{1} \sig V$,
since on both sides we have the mass of substance number~1 contained in the solution.
Division by $m$ yields:
\begin{IEEEeqnarray*}{0l}
  w = \frac{\rho_{1} \sig V}{m} = \frac{\rho_{1} \sig}{\rho}
\end{IEEEeqnarray*}
Clearly, we can rearrange this equation in different ways, so we obtain:
\begin{IEEEeqnarray}{0l+l+l}
  \label{eq:first-principles}
  w = \frac{\rho_{1} \sig}{\rho} &
  \sig = \frac{w \rho}{\rho_{1}} &
  \rho = \frac{\rho_{1} \sig}{w}
\end{IEEEeqnarray}
Hence each of the three properties (volume concentration, mass fraction, density)
can easily be computed given the other two,
as long as the density $\rho_{1}$ is known.
We call these the \term{first principle} equations.
(The last one only holds if $w > 0$.)

These equations are rarely mentioned explicitly in the literature.
For example, $\sig = \frac{w \rho}{\rho_{1}}$ is in \cite{OimlIntAlcTab} on page~5.
In principle, the relation between mass fraction and volume concentration
described by these equations can be found in
\cite{Wittenberger05} (German) in Abschnitt~4.5.8,
and similar in \cite{NylenEtAl96} (German) in Aufgabe~2.1-8 on pages~84 and~360.
In~\cite{SchwisterLeven14} (German), see equations~(1-28) and~(1-29) on page~23.\footnote{%
  In the headline and the text, it reads \enquote{Volumenanteil} (volume fraction).
  Upon request, one of the authors has tentatively classified this as an error
  and agreed that it should be \enquote{Volumenkonzentration} (volume concentration).}
The German Wikipedia page \cite{WikipediaVolumenkonzentration}
has $\sig = \frac{w \rho}{\rho_{1}}$ in the table \enquote{Zusammenhänge mit anderen Gehaltsgrößen}.

\paragraph{Density of ethanol\-/water solution.}
For solutions of ethanol (substance number~1) and water (substance number~2),
there is a more complicated equation~\cite{OimlIntAlcTab,Richtlinie76} to obtain
density given only mass fraction.
At temperature $\SI{20}{\degree}$ and with density given in $\si{\kg\per\meter^{3}}$,
this equation is as follows
with certain constants $\eli{A}{12} \in \RR$, which are listed in~\cite{OimlIntAlcTab,Richtlinie76}:
\begin{IEEEeqnarray*}{0l}
  \rhopol(w) = \sum_{k=1}^{12} A_{k} w^{k-1}
\end{IEEEeqnarray*}
We call this the \term{1976 equation}.
Define $\rhoeth \df \rhopol(1) \approx 789.239123$.

\section{Checking alcoholometric tables in PhEur}
\label{sec:check}

The \enquote{European Pharmacopoeia} (PhEur) provides alcoholometric tables,
which allow to convert between volume concentration, mass fraction, and density at~$\SI{20}{\degree}$;
see PhEur (\eg Edition~10), Volume~I, General chapters, General texts, Section~5.5.
We give a short excerpt here, so we see the structure of the table:

\begin{table}[H]
  \tablestandard
  \begin{tabular}{n{3}{1}n{3}{2}n{4}{2}}
    \toprule
    {$\% V/V$} & {$\% m/m$} & {$\rho_{20} \, (\si{\kg\per\meter^3})$} \\
    \midrule
    0.0 & 0.0  & 998.20 \\
    0.1 & 0.08 & 998.05 \\
    0.2 & 0.16 & 997.90 \\
    0.3 & 0.24 & 997.75 \\
    0.4 & 0.32 & 997.59 \\
    \\
    49.8 & 42.24 & 930.53 \\
    49.9 & 42.33 & 930.34 \\
    50.0 & 42.43 & 930.14 \\
    50.1 & 42.52 & 929.95 \\
    50.2 & 42.61 & 929.75 \\
    \\
    99.6 & 99.34 & 791.29 \\
    99.7 & 99.50 & 790.79 \\
    99.8 & 99.67 & 790.28 \\
    99.9 & 99.83 & 789.76 \\
    100.0 & 100.0 & 789.24 \\
    \bottomrule
  \end{tabular}
\end{table}

The values for volume concentration and mass fraction in the table are in percent,
that is, the actual values are obtained by dividing by~$100$.
When we specify precision by naming the number of digits after the decimal point,
we always refer to the actual values, not the values in percent.
So the first column in the table uses $3$~digits, the second column uses $4$~digits,
and the third column uses $5$~digits.
To avoid dividing by zero, we exclude the first table row from our considerations.

The first principle equations allow us to compute each column from the other two columns.
We perform these calculations and record the relative error, in different variations:
\begin{itemize}
\item Absolute difference of computed value and table value, divided by table value.
\item Absolute difference of computed value rounded to the precision of the corresponding table column
  (for example, computed volume concentration is rounded to $3$~digits after the decimal point)
  and table value, divided by table value.
  This is marked \enquote{(r)} in the following boxplot.
\item For density, we also take absolute difference of computed value and table value,
  divided by the relevant range, which is density of water minus density of ethanol.
  This is marked \enquote{(h)} in the following boxplot.
\end{itemize}

\input{PlotPhEurRelErr.tex}

Regarding computed volume concentration and computed mass fraction,
this seems acceptable,
at least if we agree that restricting to the precision of the table is reasonable,
see \enquote{Vol Conc (r)} and \enquote{Mass Frac (r)}.
But the error in computed density is higher than expected and warrants further attention.
To this end, we plot density over mass fraction:
table value with a black \enquote{+} shape,
the value computed with first principle equation with a red \enquote{x} shape,
and the value computed with the 1976 equation as a green line.
The biggest problems seem to be for mass fractions $<0.01$,
so we give a second plot only for that region.

\input{PlotPhEurDens1.00.tex}

\input{PlotPhEurDens0.10.tex}

I am not aware of any obvious explanation for those curves and jumps.
A likely candidate is the limited precision in the first and/or second table column.
However, note that the second table column is the input to the 1976 equation,
and its results (the green line) agree well with the third table column.
Explanations to this could be: the main culprit is the precision of the first column,
and/or the first principles equation for density is more sensitive to errors than the 1976 equation.

In the following section (\autoref{sec:sens}),
we show that indeed, this conjecture about sensitivity is true:
the 1976 is more robust than the first principle equations.
In the section that follows (\autoref{sec:make}),
we show results that suggest that it is not enough to increase precision in the first column,
but we also need to increase precision in the second column.

\section{Sensitivity analysis}
\label{sec:sens}

We generate random numbers $\eli{\veps}{100}$
uniformly distributed in the intervall $\intncc{\np{-5e-5}}{\np{5e-5}}$,
and for $i=1,\hdots,100$ define:
\begin{IEEEeqnarray*}{0l+l}
  w_{i} \df \frac{i}{100} & \tiw_{i} \df w_{i} + \veps_{i} \\
\end{IEEEeqnarray*}
This is intended to simulate the situation that $w_{i}$ and $\tiw_{i}$ differ only by rounding error,
where rounding is done to $4$ digits after the decimal point.
We call the $\eli{\tiw}{100}$ the \term{exact mass fractions}
and $\eli{\tiw}{100}$ the \term{noisy mass fractions}.
We plot different versions of density over mass fraction:
\begin{itemize}
\item $\rhopol(w)$ over $w$ for all $w \in \intcc{0}{1}$ as a green curve.
\item $\rhopol(\tiw_{i})$ over $w_{i}$ for all $i$ as green circles.
  This is similar to the previous item,
  but the 1976 equation is given the noisy mass fractions.
\item For each $i$ define:
  \begin{IEEEeqnarray*}{0l+l}
    \sig_{i} \df \frac{w_{i} \rhopol(w_{i})}{\rhoeth} &
    \tirho_{i} \df \frac{\rhoeth \sig_{i}}{\tiw_{i}}
  \end{IEEEeqnarray*}
  Then plot $\tirho_{i}$ over $w_{i}$ as red \enquote{x} shapes.
  It is $\tirho_{i}$ the density computed by first principles using noisy mass fraction.
  Note that the volume concentration is also computed by first principles,
  but using the exact mass fraction.
\item For each $i$ define:
  \begin{IEEEeqnarray*}{0l+l}
    \hatsig_{i} \df \round\p{\frac{w_{i} \rhopol(w_{i})}{\rhoeth}, 4} &
    \hatrho_{i} \df \frac{\rhoeth \hatsig_{i}}{w_{i}}
  \end{IEEEeqnarray*}
  Then plot $\hatrho_{i}$ over $w_{i}$ as blue \enquote{+} shapes.
  It is $\tirho_{i}$ the density computed by first principles,
  using the exact mass fraction but introducing an error in volume concentration
  by rounding to the precision used in the PhEur table.
\item For each $i$ define:
  \begin{IEEEeqnarray*}{0l}
    \hat{\tilde{\rho}}_{i} \df \frac{\rhoeth \hatsig_{i}}{\tiw_{i}}
  \end{IEEEeqnarray*}
  Then plot $\hat{\tilde{\rho}}_{i}$ over $w_{i}$ as black rectangles.
  Here the two errors (noisy mass fraction and rounded volume concentration) are combined.
\end{itemize}

\input{PlotSensDens1.00.tex}

The plot is in large part boring, but unsurprisingly,
there is something going on at the left end.
We plot the range up to $0.1$ by redefining $w_{i} \df \frac{i}{100} \ccdot \frac{1}{10}$
for $i=1,\hdots,100$:

\input{PlotSensDens0.10.tex}

Each of the errors that we introduced has a substantial effect
on the densities computed by first principles,
while the error in mass fraction has no visible impact on densities computed with the 1976 equation.
The latter is not surprising, looking at the shape of the green curve.

We do similar plots for volume concentration over mass fraction.
Based on the good results for volume concentration computed by first principles
(\autoref{sec:check}), we expect to see little error here.
For each $i$ define:
\begin{IEEEeqnarray*}{0l}
  \tisig_{i} \df \frac{\tiw_{i} \rhopol(w_{i})}{\rhoeth} \\
  \hatsig_{i} \df \frac{w_{i} \ccdot \round\p{\rhopol(w_{i}), 2}}{\rhoeth} \\
  \hat{\tilde{\sig}}_{i} \df \frac{\tiw_{i} \ccdot \round\p{\rhopol(w_{i}), 2}}{\rhoeth}
\end{IEEEeqnarray*}
Then plot $\tisig_{i}$ as red \enquote{x} shapes,
$\hatsig_{i}$ as blue \enquote{+} shapes,
and $\hat{\tilde{\sig}}_{i}$ as black rectangles, all over $w_{i}$.

\input{PlotSensVolC1.00.tex}

\input{PlotSensVolC0.10.tex}

There is no visible error.

\section{Making a more precise table}
\label{sec:make}

(... just some notes for now ...)

Takeaway: By root finding, we create a table with high precision. By
appropriate rounding, we obtain the same table as in PhEur. When we
round to 5 significant digts, which is a bit higher precision as in
PhEur, first principles computations of density show only small
errors. We call the table with 5 significant digits our final table.

Plots of differences between PhEur table and final table show
interesting curves (which could be the source for the initial
observations with the curves and jumps when computing density)
Define:
\begin{IEEEeqnarray*}{0l}
  f(w) \df \frac{w \rhopol(w)}{\rhoeth} = \frac{1}{\rhoeth} \sum_{k=1}^{12} A_{k} w^{k}
\end{IEEEeqnarray*}
Then $f(w)$ is the volumen concentration for mass fraction $w$.
Our tables are made by computing the inverse function $f^{-1}$.
So the differences, which give those funny curves, come from this equation
with $\sig = 0.001, 0.002, \hdots$:
\begin{IEEEeqnarray*}{0l}
  \round(f^{-1}(\sig), 4) - \signif(f^{-1}(\sig), 5)
\end{IEEEeqnarray*}
Perhaps we should look at the graph of $f^{-1}$ and play with different rounding parameters.
We might get an idea where the funny curves come from.

%%% TODO

% \pagebreak

% \input{PlotPhEurMassF1.00.tex}

% \input{PlotPhEurMassF0.10.tex}

% \pagebreak

% \input{PlotPhEurVolC1.00.tex}

% \input{PlotPhEurVolC0.10.tex}

\pagebreak

\pagebreak

Roots

\input{PlotMyRootsDens0.10.tex}
\input{PlotMyRoots3Dens0.10.tex}
\input{PlotMyRoots4Dens0.10.tex}
\input{PlotMyRoots5Dens0.10.tex}

\pagebreak

Finals

\input{PlotMyFinalVolC0.10.tex}
\input{PlotMyFinalMassF0.10.tex}
\input{PlotMyFinalDens0.10.tex}

\input{PlotMyFinalVolC1.00.tex}
\input{PlotMyFinalMassF1.00.tex}
\input{PlotMyFinalDens1.00.tex}

\pagebreak

\input{PlotMyFinalRelErr.tex}

\pagebreak

Comparison PhEur and our Finals

\input{PlotCompareDiff1.00.tex}

\input{PlotCompareDiff0.10.tex}

\pagebreak

\input{PlotCompareRelDiff1.00.tex}

\input{PlotCompareRelDiff0.10.tex}

\pagebreak

\input{PlotCompareDens1.00.tex}

\input{PlotCompareDens0.10.tex}

\pagebreak

\printbibliography

\pagebreak
\section{Version History}
\label{sec:version-history}

\begin{description}
\item[CHANGEME:] first release, request for comments
\end{description}

\pagebreak\thispagestyle{empty}
\input{lxklCreativeCommons}
\input{lxklNoPaper}

\end{document}
