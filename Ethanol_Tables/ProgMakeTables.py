from mpmath import mp, mpf, nstr, polyroots, polyval
from decimal import Decimal

## For mp.dps, any value of 52 or greater seems fine, if VC_REL_EPS is
## adjusted accordingly. For example, mp.dps = 52 and VC_REL_EPS =
## 1e-50 is a working combination.
mp.dps = 100
VC_REL_EPS = 1e-98
TSV_PREC = 30

def read_coefficients(filename):
	with open(filename, 'r') as file:
		coefficients = [mpf(str(Decimal(line.strip()))) for line in file]
	return coefficients[::-1]

DATA_1976_VECTOR_A = read_coefficients('Data1976VectorA.txt')

def mass_fraction_to_density(mf):
	return polyval(DATA_1976_VECTOR_A, mf)

## With a C++ program, I computed 789.2391232999144 for the density of
## ethanol. Here, when I print the computed value, I get 789.2391233,
## then loads of zeros, and then a 7, which - ignoring the loads of
## zeros and the 7 - is the C++ value rounded to 7 digits after the
## decimal point.
DENSITY_ETHANOL = mass_fraction_to_density(1)
assert mp.almosteq(DENSITY_ETHANOL, mpf('789.2391233'), rel_eps=1e-99)

def mass_fraction_to_volume_concentration(mf):
	return mpf(mf) * mass_fraction_to_density(mf) / DENSITY_ETHANOL

def volume_concentration_to_mass_fraction(vc):
	all_roots = polyroots(DATA_1976_VECTOR_A + [-mpf(vc) * DENSITY_ETHANOL])
	relevant_roots = [root for root in all_roots if 0 <= root.real and root.real <= 1.0 and root.imag == 0]
	assert(len(relevant_roots) == 1)
	res = relevant_roots[0]
	assert mp.almosteq(mass_fraction_to_volume_concentration(res), mpf(vc), rel_eps=VC_REL_EPS)
	return res

def make_table():
	with open('DataTableMyRoots.tsv', 'w') as file:
		file.write('VolC\tMassF\tDens\n')
		for i in range(1, 1001):
			vc = mpf(i) / mpf(1000);
			mf = volume_concentration_to_mass_fraction(vc)
			dens = mass_fraction_to_density(mf)
			file.write('{}\t{}\t{}\n'.format(nstr(mpf(100)*vc, TSV_PREC),
											 nstr(mpf(100)*mf, TSV_PREC),
											 nstr(dens,        TSV_PREC)))

make_table()
