library(dplyr)

args <- commandArgs(trailingOnly=TRUE)
FileName <- ifelse(is.na(args[1]), "DataTableMyRoots.tsv", args[1])

read.delim(FileName) |>
    mutate(VolC = round(VolC, digits=3),
           MassF = signif(MassF, digits=5),
           Dens = round(Dens, digits=2)) |>
    write.table(file="DataTableMyFinal.tsv", sep="\t", row.names=FALSE)
